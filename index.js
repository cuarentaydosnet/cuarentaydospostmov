const argon2 = require('argon2');
const requestJson = require('request-json');
const uuid = require('uuid/v4');

exports.handler = function(event,context,callback){
    console.log("POST movimientos");
    const user = event.requestContext.authorizer;
    const apikey = event.stageVariables.mLabAPIKey;
    const db = event.stageVariables.baseMLabURL;
    const mitoken = event.stageVariables.tokenA1;
    const api = event.stageVariables.api
    
    let body = JSON.parse(event.body);
    console.log(body)
    var newMov = {
      "id" : uuid(),
      "concepto" : body.concepto,
      "importe" : body.importe,
      "fecha" : Date.now(),
      "tipo" : body.tipo
    };
    console.log(newMov);
    // comporbamos el saldo
    var cliente = requestJson.createClient(api)
    cliente.headers['Authorization'] = mitoken;
    cliente.headers['Content-Type'] = 'application/json';
    var consulta ="cuentas/" + event.pathParameters.accountid + "/saldo"
    cliente.get(consulta, function(err,res,body){
        if(err){
            var response = {
                "statusCode" : 501,
                "headers" : {"Access-Control-Allow-Origin": "*"}, 
                "body": JSON.stringify(err)
            };
            return callback(null,response);
        } else {
            var saldo=body.saldo;  
            console.log(newMov.tipo)
            if(newMov.tipo == 0 && (saldo - newMov.importe < 0)){
                //no hay saldo
                console.log("no hay saldo")
                var response = {
                     "statusCode" : 404,
                     "headers" : {"Access-Control-Allow-Origin": "*"},      
                     "body": "{\"msg\" : \"No hay saldo suficiente\"}"
                    }
                return callback(null,response);
            } else {
                console.log("Hay saldo")
                var httpClient = requestJson.createClient(db);
                var query="account?q={\"_id\": \""+ event.pathParameters.accountid + "\"}&" + apikey
                console.log(query)
                httpClient.get(query, function(err,resMLab,cuenta){
                    console.log(err)
                     if(err){
                         var response = {
                             "statusCode" : 501,
                             "headers" : {"Access-Control-Allow-Origin": "*"},      
                             "body": "{\"msg\" : \"Error al persistir el dato\"}"
                         }
                         return callback(null,response);
                     } else {
                         var micuenta = cuenta[0]
                         var movimientos = micuenta.movimientos
                         movimientos.push(newMov)
                         var newdocument = '{ "$set" :{ "movimientos" : ' + JSON.stringify(movimientos) + ' }}';
                         console.log(newdocument + typeof newdocument)
                         httpClient.put("account/" + micuenta._id+ "?" + apikey, JSON.parse(newdocument), function(err,resMLab,body){
                             if(err){
                                 var response = {
                                     "statusCode" : 501,
                                     "headers" : {"Access-Control-Allow-Origin": "*"},      
                                     "body": JSON.stringify(err)
                                 }
                             } else {
                                 var response = { 
                                     "statusCode": 200,
                                    "headers" : {"Access-Control-Allow-Origin": "*"},                             
                                     "body": JSON.stringify(newMov),
                                     "isBase64Encoded": false
                                };
                             }
                             return callback(null,response);
                         })
                    } 
                })
            }
        }
    })
}


